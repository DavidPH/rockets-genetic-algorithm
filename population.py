from rocket import Rocket
import random


class Population:
    def __init__(self, mutation_rate, size, screen, target):
        self.screen = screen
        self.pop_size = size
        self.generation = 0
        self.mutation_rate = mutation_rate
        self.target = target
        self.rockets = [Rocket(screen, self.target) for _ in range(self.pop_size)]
        self.fitness_lst = None
        self.pool = []

    def has_alive(self):
        for rocket in self.rockets:
            if rocket.is_alive:
                return True
        return False

    def get_fitness_lst(self):
        self.fitness_lst = [self.rockets[i].dna.get_fitness(self.target.pos, self.rockets[i].position) for i in
                            range(self.pop_size)]
        return self.fitness_lst

    def pick_one(self):
        self.fitness_lst = self.get_fitness_lst()
        sum_fitness = int(sum(self.fitness_lst))
        r = random.randrange(0, sum_fitness + 1)
        i = 0
        while r > 0:
            r = r - self.fitness_lst[i]
            i += 1
        i -= 1
        return self.rockets[i]

    def pick_best(self):
        self.fitness_lst = self.get_fitness_lst()
        max_fit = (None, 0)
        for i, fitness in enumerate(self.fitness_lst):
            if fitness >= max_fit[1]:
                max_fit = (i, fitness)
        return self.rockets[max_fit[0]]

    def new_generation(self):
        new_pop = []
        for i in range(self.pop_size):
            a = self.pick_one()
            b = self.pick_one()
            dna = a.dna.crossover(b.dna).mutate(self.mutation_rate)
            new_pop.append(Rocket(self.screen, self.target, dna.genes))
        self.rockets = new_pop
        self.fitness_lst = None
        self.generation += 1
        return self.generation
