import pygame as pg
import math
import random
from dna import DNA

vec = pg.math.Vector2


class Rocket(pg.sprite.Sprite):

    def __init__(self, screen, target, dna=None):
        super().__init__()
        self.screen = screen
        self.target = target
        r = self.screen.get_rect()
        self.screen_w, self.screen_h = r.width, r.height
        self.image = pg.image.load("rocket_sprite.png").convert()
        self.image = pg.transform.scale(self.image, (10, 35))
        self.image.set_colorkey((255, 255, 255))
        self.original_image = self.image
        self.rect = self.image.get_rect()
        self.position = vec(self.screen_w / 2, self.screen_h - self.rect.height)
        self.rect.midbottom = self.position

        self.dna = DNA(dna)
        self.is_alive = True

        self.vel = vec(0, 0)
        self.accel = vec(0, -0.1)
        self.max_speed = 5
        self.angle_speed = 0
        self.angle = 0

        self.midtop = vec(self.rect.midtop)

    def manual_move(self):
        keys = pg.key.get_pressed()

        if keys[pg.K_LEFT]:
            self.angle_speed = -2
            self.rotate()

        if keys[pg.K_RIGHT]:
            self.angle_speed = 2
            self.rotate()

        if keys[pg.K_UP]:
            self.vel += self.accel
        if keys[pg.K_DOWN]:
            self.vel -= self.accel


    def rotate(self):
        self.accel.rotate_ip(self.angle_speed)
        self.angle += self.angle_speed
        if self.angle > 360:
            self.angle -= 360
        elif self.angle < 0:
            self.angle += 360

        px, py = self.rect.midtop
        ox, oy = self.rect.center
        angle = math.radians(self.angle)
        midtop = (ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy),
                  oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy))
        self.midtop = vec(midtop)
        self.image = pg.transform.rotate(self.original_image, -self.angle)
        self.rect = self.image.get_rect(center=self.rect.center)

    def update(self):
        self.manual_move()
        if pg.sprite.collide_rect(self, self.target):
            self.kill()
            self.dna.health += 2000
        count = self.dna.get_health()
        if self.is_alive:
            if count is not None:
                angle, thrusters = self.dna.genes[count]
                if thrusters:
                    self.vel += self.accel
                self.angle_speed = angle
                self.rotate()
            else:
                self.kill()

            if self.vel.length() > self.max_speed:
                self.vel.scale_to_length(self.max_speed)

            if self.rect.right + self.vel.x > self.screen_w or self.rect.left + self.vel.x < 0:
                self.vel.x = 0
                self.kill()

            if self.rect.bottom + self.vel.y > self.screen_h or self.rect.top + self.vel.y < 0:
                self.vel.y = 0
                self.kill()
            self.midtop += self.vel
            self.position += self.vel
            self.rect.center = self.position

    def kill(self):
        super().kill()
        self.is_alive = False
        self.dna.health = 0
        # print("Dead")
