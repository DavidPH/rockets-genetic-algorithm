import random
import math


class DNA:
    def __init__(self, genes=None):
        self.lifespan = 200
        self.health = self.lifespan
        self.fitness = 0
        if not genes:
            self.genes = [self._random_gene() for _ in range(self.lifespan)]
        else:
            self.genes = genes

    @staticmethod
    def _random_gene():
        # return random.randrange(-5, 5), random.choice([True, False])
        return random.choice([-2, 2]), random.choice([True, False])
        # return 1, random.choice([True, False])

    def get_health(self):
        i = self.lifespan - self.health
        if i < self.lifespan:
            self.health -= 1
            return i
        else:
            return None

    def get_fitness(self, loc, des):
        px, py = loc
        ox, oy = des

        self.fitness = math.sqrt((ox - px) ** 2 + (oy - py) ** 2)

        self.fitness = (1 / self.fitness) * (self.health + 1) * 100
        return self.fitness

    def crossover(self, other):
        mid_point = random.randrange(0, self.lifespan)
        genes = [(None, None) for _ in range(self.lifespan)]
        for i in range(self.lifespan):
            if i > mid_point:
                genes[i] = self.genes[i]
            else:
                genes[i] = other.genes[i]
        return DNA(genes)

    def mutate(self, rate):
        for i in range(self.lifespan):
            if random.random() < rate:
                # if random.random() < rate:
                self.genes[i] = self._random_gene()
        return self
