import sys
from rocket import *
from population import Population

RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)


class Target(pg.sprite.Sprite):
    def __init__(self, screen, pos):
        super().__init__()
        self.screen = screen
        self.pos = pos
        self.image = pg.Surface((30, 30), pg.SRCALPHA)
        self.rect = self.image.get_rect()
        pg.draw.circle(self.image, GREEN, (int(self.rect.width / 2), int(self.rect.height / 2)),
                       int(self.rect.width / 2))
        self.rect.center = self.pos

    def draw(self):
        self.screen.blit(self.image, self.rect)


class App:
    def __init__(self, w, h):

        # Create screen
        self.screen_width, self.screen_height = w, h
        self.screen = pg.display.set_mode((self.screen_width, self.screen_height))
        pg.display.set_caption('Rockets - Genetic Algorithm')
        # Create white background
        self.background = pg.Surface((self.screen_width, self.screen_height)).convert()
        self.background.fill((255, 255, 255))
        # Initialize clock
        # self.clock = pg.time.Clock()
        # Creating Sprites
        self.target = Target(self.screen, (random.randrange(30, self.screen_width - 30), 30))
        self.population = Population(0.01, 500, self.screen, self.target)
        self.rocket_sprites = pg.sprite.Group()
        self.rocket_sprites.add([self.population.rockets])

    def main_loop(self):

        while True:
            # Get loop time, convert milliseconds to seconds
            # tick = self.clock.tick(60) / 1000

            if not self.population.has_alive():
                print(self.population.pick_best().dna.fitness)
                pg.display.set_caption(f"Generation: {self.population.new_generation()}")
                self.rocket_sprites.add(self.population.rockets)

            # Handle input
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    sys.exit(0)

                if event.type == pg.MOUSEBUTTONDOWN:
                    self.target.pos = pg.mouse.get_pos()
                    self.target.rect.center = pg.mouse.get_pos()

            # Erase previous frame by bliting background
            self.screen.blit(self.background, self.background.get_rect())

            # Update the entities in your game

            self.rocket_sprites.update()
            self.target.update()

            # Blit all your entities
            self.rocket_sprites.draw(self.screen)
            self.target.draw()

            # Update display
            pg.display.update()


if __name__ == '__main__':
    app = App(800, 600)
    pg.init()
    app.main_loop()
    pg.quit()
